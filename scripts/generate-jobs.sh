#!/usr/bin/env bash

set -euo pipefail

cat <<EOF
stages:
- dummy

stage_one:job_one:
  extends:
  - .job_one

stage_two:job_two:
  extends:
  - .job_two
  needs:
  - stage_one:job_one

EOF